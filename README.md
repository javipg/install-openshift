# Scripts para instalaci�n de Openshift Origin 3.6 sobre VBox para un entorno de laboratorio

## Descripci�n

Este repo contiene todos los scripts y pasos necesarios para instalar un cluster de Openshift Origin 3.6 sobre Virtual Box con finalidades de laboratorio. 

Bajo ning�n concepto debe considerarse una instalaci�n productiva.

## Entorno

Se ha realizado la instalaci�n sobre un entorno con las siguientes caracter�sticas.

* Intel Core i5-6500 3,2Ghz
* 64 Gb de RAM
* Windows 10 x64
* Virtual Box 5.1.30 r118389 (Qt5.6.2)
* Vboxes con CentOS-7-x86_64-Minimal-1611

Sobre dicha infraestructura se ha creado 6 vboxes; 1 NFS Server, 1 Master, 2 Infra Node y 2 App Node con las siguientes caracter�sticas.

| VBox                 | RAM    | Disco duro | NetworkAdapter   | IP            | DNS                 | GATEWAY     | DOMAIN      | SUBNET         |
|:--------------------:|:------:|:----------:|:----------------:|:-------------:|:-------------------:|:-----------:|:-----------:|:--------------:|
| dns.jrovira.org      | 1Gb    | 150 Gb     | Bridge           | 192.168.1.10  | dns.jrovira.org     | 192.168.1.1 | jrovira.org | 192.168.1.0/24 |
| nfs.jrovira.org      | 1Gb    | 150 Gb     | Bridge           | 192.168.1.20  | nfs.jrovira.org     | 192.168.1.1 | jrovira.org | 192.168.1.0/24 |
| gluster1.jrovira.org | 1Gb    | 150 Gb     | Bridge           | 192.168.1.21  | gluster1.jrovira.org| 192.168.1.1 | jrovira.org | 192.168.1.0/24 |
| gluster2.jrovira.org | 1Gb    | 150 Gb     | Bridge           | 192.168.1.22  | gluster2.jrovira.org| 192.168.1.1 | jrovira.org | 192.168.1.0/24 |
| master.jrovira.org   | 8 Gb   | 150 Gb     | Bridge           | 192.168.1.50  | master.jrovira.org  | 192.168.1.1 | jrovira.org | 192.168.1.0/24 |
| infra.jrovira.org    | 8 Gb   | 150 Gb     | Bridge           | 192.168.1.100 | infra.jrovira.org   | 192.168.1.1 | jrovira.org | 192.168.1.0/24 |
| infra2.jrovira.org   | 8 Gb   | 150 Gb     | Bridge           | 192.168.1.101 | infra2.jrovira.org  | 192.168.1.1 | jrovira.org | 192.168.1.0/24 |
| app2.jrovira.org     | 8 Gb   | 150 Gb     | Bridge           | 192.168.1.150 | app.jrovira.org     | 192.168.1.1 | jrovira.org | 192.168.1.0/24 |
| app2.jrovira.org     | 8 Gb   | 150 Gb     | Bridge           | 192.168.1.151 | app2.jrovira.org    | 192.168.1.1 | jrovira.org | 192.168.1.0/24 |

Por problemas con la instalaci�n de Openshift, se ha tenido que crear vboxes con un solo adaptador de red en modo bridge, dado que las combinaciones de 1 adaptador Nat Network o 2 adaptadores Nat Network + Host Only produjeron errores.

La limitaci�n de esta instalaci�n es que depende que la red del PC donde instalar se encuentre en la 192.168.1.0/24 y tenga las IPs mencionadas libres para ser asignadas estaticamente a las vboxes.

## Preparaci�n de las vboxes

Se crean las 6 vboxes citadas con las caracter�sticas mencionadas y se realiza en cada una de ellas la instalaci�n indicando en el proceso.

* IP est�tica
* Netmask 255.255.255.0
* Gateway 192.168.1.1
* DNS Server; 192.168.1.10 y 8.8.8.8 (este �ltimo para acceso a internet)
* Domain: jrovira.org
* Asignaci�n del password de root

Tambi�n es posible crear una vbox y luego exportarla como OVA e importarla 5 veces (una para cada m�quina de las restantes), pero ser� necesario modificar la configuraci�n del adaptador de red, de manera que quede como el siguiente:

```
TYPE="Ethernet"
BOOTPROTO="none"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
IPV6_ADDR_GEN_MODE="stable-privacy"
NAME="enp0s3"
UUID="bb2c5cf2-9b01-4000-8863-330ed9c9c692"
DEVICE="enp0s3"
ONBOOT="yes"
IPADDR="192.168.1.50" (poner la IP que corresponda a la maquina)
PREFIX="24"
GATEWAY="192.168.1.1"
DNS1="192.168.1.10"
DNS2="8.8.8.8"
IPV6_PEERDNS="yes"
IPV6_PEERROUTES="yes"
IPV6_PRIVACY="no"
```

Si actualizamos el adaptador de red manualmente (/etc/sysconfig/network-adapter/ifcfg-enp0s3) tras guardar los cambios deberemos ejecutar lo siguiente:

```
> systemctl restart network NetworkManager
```

Si se opta por importar una imagen OVA para no tener que instalar 6 veces el sistema operativo, durante el proceso de importaci�n es importante marcar la opci�n que se resetee la direcci�n MAC del adaptador de red, sino entrar� en colisi�n con el de las otras m�quinas generadas.

## Configuraci�n previa

Una vez hemos creado las 6 vboxes, procederemos a instalar y configurar unos pasos comunes a todas ellas. Para ello ejecutaremos los siguientes scripts

```
> yum install -y net-tools telnet git
...
> git clone https://rovirajoel@bitbucket.org/jrlab/install-openshift.git
...
> cd openshift-install/common
...
> ./01.create_user.sh *<usuario no root>*
...
> ./02.install_base.sh
...
> ./03.config_ssh_for_root.sh
...
> su *<usuario no root>*
> ./04.config_ssh_for_user.sh
...
> exit *_(volvemos al usuario root)_*
> ./05.config_hostname.sh *<xxx.jrovira.org>* _(donde xxx es alguno de los valores de la tabla mencionada anteriormente)_
...
```

## Configuraci�n del servidor de DNS

Una vez tenemos todas las m�quinas con los pasos anteriores realizados, sobre la vbox "dns.jrovira.org" ejecutaremos lo siguiente:

```
> cd openshift_install/dns_server
> ./01.install_config_dns_server.sh
```

## Configuraci�n del servidor de NFS

Para realizar la instalaci�n del servidor de NFS, sobre la vbox "nfs.jrovira.org" ejecutaremos lo siguiente:

```
> cd openshift_install/nfs
> ./01.install.sh
```

## Configuraci�n del servidor de GlusterFS

Para realizar la instalaci�n del servidor de GlusterFS, sobre las vboxs "glusterX.jrovira.org" ejecutaremos lo siguiente:

```
> cd openshift_install/glusterfs
> ./01.install.sh
```

## Configuraci�n de requisitos de Openshift

Sobre el master, los nodos de app e infra deberemos ejecutar lo siguiente

```
> cd openshift_install/openshift/common
> ./01.install_openshift_reqs.sh
```

## Instalaci�n de Openshift Origin 3.6

Y por �ltimo, en el master deberemos ejecutar

```
> cd openshift_install/openshift/master
> ./01.config_ssh_access.sh
...
> ./02.install_openshift.sh *<usuario no root>* *<password usuario no root>*
...
```

Tras una hora aproximadamente de instalaci�n, deber�amos tener Openshift instalado.
