yum -y install nfs-util

# Configuración del NFS
cp idmapd.conf /etc/idmapd.conf
cp exports /etc/exports

# Creacion del directorio de NFS
mkdir /nfs

# Configuración del firewall
firewall-cmd --add-service=nfs --permanent 
firewall-cmd --reload

# Reiniciamos el servidor NFS
systemctl start rpcbind nfs-server 
systemctl enable rpcbind nfs-server
