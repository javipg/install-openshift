yum install -y wget bind-utils iptables-services bridge-utils bash-completion kexec sos psacct httpd-tools
yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sed -i -e "s/^enabled=1/enabled=0/" /etc/yum.repos.d/epel.repo
yum -y --enablerepo=epel install ansible pyOpenSSL
cd ~
git clone https://github.com/openshift/openshift-ansible
yum install -y docker
systemctl restart docker.service
docker version